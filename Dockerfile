FROM ubuntu

RUN apt-get update

# Install CURL and PostgreSQL
RUN apt-get update && \
    apt-get install -y curl && \
    apt-get install -y postgresql postgresql-contrib && \
    apt-get clean

# Install node.js
RUN cd ~ && \
    curl -sL https://deb.nodesource.com/setup_20.x -o /tmp/nodesource_setup.sh && \
    bash /tmp/nodesource_setup.sh && \
    apt-get install -y nodejs && \
    apt-get upgrade -y && \
    apt-get clean

# Copy packge.json file and install dependencies
WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm i --force

COPY . .

# Initialize database
RUN service postgresql start && \
    su - postgres -c "psql -U postgres -d postgres -c \"alter user postgres with password 'password';\"" && \
    su - postgres -c "psql -U postgres -d postgres -c \"create database express_knex;\"" && \
    npm run knex:migrate:latest

RUN chmod +x ./docker_start.sh

ENTRYPOINT [ "./docker_start.sh" ]


# docker build -t <image_name> <path/to/Dockerfile>
# docker build -t <image_name> .
# docker run -it -p port:port <image_name>

# make container form this image where data is persist in volume
# docker run -dp 8090:8090 --name <container_name> -v <volume_name>:<path/to/db/in/container> <image_name>
# docker run -dp 8090:8090 --name express-crash-cont -v express-crash-vol:/var/lib/postgresql express-crash
