import bcrypt from "bcrypt";

const saltRounds = 10;
let hashedPassword = "";

const plainPassword = "KJHJKiutyf%#gfc&^vhgct$W#cgf%$GDbvkdjsbfuT&%R^hgcksdfj7tgutf8r345KJHK*&^JHV%Efc*jbkY56GFCGFXGFXvc*&kjbjgj&%gfcgfxdfbcbY^Rlkknkj&^%bjbj*&^nMBHJGU7%&6FGCT^R&hvbV6rt7GFC&rcfgYdHV%RgcYrVgYrVGYRVytd6G%ERV&^HVG&^HBIUGhv67bvhgYTB^NT";


bcrypt.hash(plainPassword, saltRounds, async (_err, cipher) => {
  hashedPassword = cipher;

  const modifiedPassword = "KJHJKiutyf%#gfc&^vhgct$W#cgf%$GDbvkdjsbfuT&%R^hgcksdfj7tgutf8r345KJHK*&^"

  const match = await bcrypt.compare(modifiedPassword, hashedPassword);

  console.log("Password match:", match);
});




/**
 * @finding password length for bcrypt < 72
 */
