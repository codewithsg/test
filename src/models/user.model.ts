import { bookshelf } from "../db/db";

export default class User extends bookshelf.Model<User> {
  get tableName() {
    return "users";
  }

  static async isExists(username: string) {
    return await new User().where({ username }).fetch({ require: false });
  }
}
