import { bookshelf } from "../db/db";

export default class PostModel extends bookshelf.Model<PostModel> {
  get tableName() {
    return "posts";
  }

  static async fetchById(id: number): Promise<PostModel | null> {
    return await new PostModel().where({ id }).fetch({ require: false });
  }

  static async fetchPosts(offset: number, limit: number) {
    return await new PostModel().query(qb => {
      // @ts-ignore
      qb.offset(offset).limit(limit);
    }).fetchAll();
  }
}
