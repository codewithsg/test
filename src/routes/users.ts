import { Router } from "express";
import { createUser, loginUser } from "../controllers/user.controller";
import isUserExists from "../middlewares/isUserExist";

const users = Router();

users.post("/", isUserExists, createUser);
users.post("/login", isUserExists, loginUser);

export default users;
