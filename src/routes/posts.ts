import { Router } from "express";
import { createPost, deletePost, getPost, getPosts, updatePost } from "../controllers/post.controller";
import verifyUser from "../middlewares/verityUser";

const posts = Router();

posts.use(verifyUser);

// Get all posts
posts.get('/', getPosts);

// Get post by id
posts.get('/:id', getPost);

// Create new post
posts.post('/', createPost);

// update post
posts.put('/:id', updatePost);

// delete post
posts.delete('/:id', deletePost)

export default posts;
