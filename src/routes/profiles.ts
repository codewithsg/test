import { Router } from "express";
import { uploadToDb } from "../utils/upload";
import { createProfile } from "../controllers/profile.controller";

const profiles = Router();

profiles.post("/", uploadToDb.single("profile"), createProfile);

export default profiles;
