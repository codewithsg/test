import express from "express";
import posts from "./routes/posts";
import logger from "./middlewares/logger";
import error from "./middlewares/error";
import notFound from "./middlewares/notFound";
import users from "./routes/users";
import profiles from "./routes/profiles";
import helmet from "helmet";
import cors from "cors";

const port = process.env.PORT || 8090;

const app = express();

// User helmet middleware
app.use(helmet());
app.use(cors({
  origin: "http://127.0.0.1",
  optionsSuccessStatus: 200
}))

// Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// logger middleware
app.use(logger);

app.use('/api/posts', posts);
app.use('/api/users', users);
app.use('/api/profiles', profiles);

// not found handler middleware
app.use(notFound);

// error handler
app.use(error);

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
});
