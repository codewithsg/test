import { NextFunction, Request, Response } from "express";
import MyErr from "../types/error";

export default function error(err: MyErr, _req: Request, res: Response, _next: NextFunction) {
  if (err.status) {
    res.status(err.status).json({ message: err.message });
  } else {
    res.status(500).json({ message: err.message });
  }
}
