import { NextFunction, Request, Response } from "express";
import { verifyJWT } from "../utils/jwt";
import MyErr from "../types/error";
import db from "../db/db";

export default async function verifyUser(req: Request, _res: Response, next: NextFunction) {
  const token = <string>req.headers.authorization;
  const tokenVerification = verifyJWT(token);

  if (typeof(tokenVerification) === "string") {
    const error: MyErr = new Error(tokenVerification);
    error.status = 404;
    next(error);
  } else {
    const user = await db("users").select("*").where({ id: tokenVerification.id, username: tokenVerification.username }).first();
    
    if (!user) {
      const error: MyErr = new Error('User does not exist');
      error.status = 404;
      next(error);
    } else {
      next();
    }
  }
}
