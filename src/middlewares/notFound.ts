import { NextFunction, Request, Response } from "express";
import MyErr from "../types/error";

export default function notFound(_req: Request, _res: Response, next: NextFunction) {
  const error: MyErr = new Error("Not Found");
  error.status = 404;
  next(error);
}
