import { NextFunction, Request, Response } from "express";
import User from "../models/user.model";

/**
 * @description Set `req.body.user` to `user` if the user exists in the database otherwise `null`
 */
export default async function isUserExists(req: Request, _res: Response, next: NextFunction) {
  const username = req.body.username;

  if (username) {
    const user = await User.isExists(username);

    if (!user) req.body.user = null;
    else req.body.user = user.toJSON();

    next();

  } else {
    next();
  }
}
