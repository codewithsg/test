export default interface MyErr extends Error {
  status?: number;
}
