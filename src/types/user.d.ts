export default interface IUser {
  id?: string;
  username: string;
  email: string;
  password: string;
}

export type TNewUser = Omit<IUser, 'id'>;
