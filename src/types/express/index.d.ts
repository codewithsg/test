import { TNewUser } from "../user";

declare global {
  namespace Express {
    export interface Request {
      user?: TNewUser | null;
    }
  }
}

export {};
