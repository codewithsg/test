import { NextFunction, Request, Response } from "express";
import db from "../db/db";

const table = "profiles";

export async function createProfile(req: Request, res: Response, _next: NextFunction) {
  const profile = req.file ? req.file.buffer : null;

  try {
    const response = await db(table).insert({ img: profile }).returning("*");
    res.status(201).json(response);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
}
