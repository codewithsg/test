import { NextFunction, Request, Response } from "express";
import { TNewUser } from "../types/user";
import MyErr from "../types/error";
import { generateJWT } from "../utils/jwt";
import { comparePassword, hashPassword } from "../utils/password";
import User from "../models/user.model";


export async function createUser(req: Request, res: Response, next: NextFunction) {
  const user: TNewUser = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  }

  if (user.username && user.email && user.password) {
    if (req.body.user !== null) {
      const error: MyErr = new Error(`Username '${user.username}' is already in use!`);
      error.status = 400;
      next(error);
    } else {
      if (user.password.length < 72) {
        user.password = hashPassword(user.password);
        const newUser = (await new User().save(user)).toJSON();

        delete newUser.password;
        newUser.token = generateJWT({ id: newUser.id, username: user.username });
        res.json(newUser);
      } else {
        const error: MyErr = new Error(`Password length must be less than 72`);
        error.status = 400;
        return next(error);
      }
    }
  } else {
    const error: MyErr = new Error(`Please include a valid details!`);
    error.status = 400;
    return next(error);
  }
}


// sent JWT if user verifyed
export async function loginUser(req: Request, res: Response, next: NextFunction) {
  const user = req.body.user;

  if (user !== null) {
    const password = req.body.password;

    if (!password) {
      const error: MyErr = new Error(`Please insert your password`);
      error.status = 200;
      next(error);
    } else {
      const hashedPassword: string = user.password;
      const match = await comparePassword(password, hashedPassword)

      if (match) {
        delete user.password;
        user.token = generateJWT({ id: user.id, username: user.username })
        res.json(user);
      } else {
        const error: MyErr = new Error("Password doesn't match! (Missing or Incorrect Credentials)");
        error.status = 401;
        next(error);
      }
    }
  } else {
    const error: MyErr = new Error(`User doesn't exists!`);
    error.status = 404;
    next(error);
  }
}
