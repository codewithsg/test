import { NextFunction, Request, Response } from "express";
import MyErr from "../types/error";
import Post from "../types/post";
import PostModel from "../models/post.model";

/**
 * @description GET all posts
 * TODO::: take limit and page in query
 */
export async function getPosts(req: Request, res: Response) {
  let offset = Number(req.query.offset);
  let limit = Number(req.query.limit);

  // set default offset and limit
  if (isNaN(offset)) offset = 0;
  if (isNaN(limit)) limit = 100;

  const posts = await PostModel.fetchPosts(offset, limit);
  res.json(posts);
}


/**"posts"
 * @description Get single post
 */
export async function getPost(req: Request, res: Response, next: NextFunction) {
  try {
    const id = Number(req.params.id);
    const post = await PostModel.fetchById(id);

    if (post) return res.json(post);

    const error: MyErr = new Error(`Post with ID ${id} does not exist`);
    error.status = 404;
    next(error);
  } catch (error) {
    res.status(500).json({ error });
  }
}


/**
 * @description Create new post
 */
export async function createPost(req: Request, res: Response, next: NextFunction) {
  let newPost: Post = {
    title : req.body.title
  }

  if (!newPost.title) {
    const error: MyErr = new Error(`Please include a valid title`);
    error.status = 400;
    return next(error);
  }

  newPost = (await new PostModel().save({ title: newPost.title })).toJSON()
  res.status(201).json(newPost);
}


/**
 * @description Update a post
 */
export async function updatePost(req: Request, res: Response, next: NextFunction) {
  try {
    const id = Number(req.params.id);
    const post = await PostModel.fetchById(id);

    if (!post) {
      const error: MyErr = new Error(`Post with ID ${id} does not exist`);
      error.status = 404;
      return next(error);
    }

    await post.save({ title: req.body.title }, { patch: true });

    res.status(200).json(post);
  } catch (error) {
    res.status(200).json({ error });
  }
}


/**
 * @description Delete a post
 */
export async function deletePost(req: Request, res: Response, next: NextFunction) {
  try {
    const id =  Number(req.params.id);
    const post = await PostModel.fetchById(id);

    if (!post) {
      const error: MyErr = new Error(`Post with ID ${id} does not exist`);
      error.status = 404;
      return next(error);
    }

    await post.destroy();

    res.status(200).json(post);
  } catch (error) {
    res.status(200).json({ error });
  }
}
