import dotenv from "dotenv";
import type { Knex } from "knex";

dotenv.config();
dotenv.config({ path: "../.env" });

const config: { [key: string]: Knex.Config } = {
  development: {
    client: "pg",
    connection: process.env.PG_DB_CONNECTION_STRING,
    migrations: { directory: "./db/migrations" },
    seeds: { directory: "./db/seeds" },
  },

  testing: {
    client: "pg",
    connection: process.env.PG_DB_CONNECTION_STRING,
    migrations: { directory: "./db/migrations" },
    seeds: { directory: "./db/seeds" },
  },

  production: {
    client: "pg",
    connection: process.env.PG_DB_CONNECTION_STRING,
    migrations: { directory: "./db/migrations" },
    seeds: { directory: "./db/seeds" },
  }
};

export default config;
