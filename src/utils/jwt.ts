import jwt from "jsonwebtoken";

interface Payload {
  id: string;
  username: string;
}

const secretKey = <string>process.env.PRIVATE_KEY;
const options: jwt.SignOptions = {
  algorithm: "HS256",
  expiresIn: "5m",
};

export function generateJWT(payload: Payload): string {
  return jwt.sign(payload, secretKey, options);
}

export function verifyJWT(token: string): Payload | string {
  try {
    const decoded: any = jwt.verify(token, secretKey);
    return { id: decoded.id, username: decoded.username };
  } catch (err: any) {
    // err_msg == "invalid signature" is token is invalid
    // err_msg == "jwt expired" if token is expired
    if (err.message === "invalid signature" || err.message === "invalid token") return "Invalid token";
    else if (err.message === "jwt expired") return "Token expired";
    else return "Token error";
  }
}
