import bcrypt from "bcrypt";

const saltRounds = 10;

export function hashPassword(password: string) {
  let hash: string = bcrypt.hashSync(password, saltRounds);
  return hash;
}

export async function comparePassword(password: string, hash: string): Promise<boolean> {
  const match: boolean = await bcrypt.compare(password, hash);
  return match;
}
