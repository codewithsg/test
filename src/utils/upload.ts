import { Request } from "express";
import multer from "multer";

/*TODO::: check mime type for image and ignore others  */

const storage = multer.diskStorage({
  destination: (_req, _file, cb) => { cb(null, "uploads/" )},
  filename: (_req, file, cb) => { cb(null, Date.now() + "_" + file.originalname) },
});

const memory = multer.memoryStorage();

function fileFilter(_req: Request, file: Express.Multer.File, cb: multer.FileFilterCallback) {
  const allowedTypes = ['image/jpeg', 'image/png'];

  if (!allowedTypes.includes(file.mimetype)) {
    const error = new Error("Invalid file type");
    // @ts-ignore
    return cb(error, false);
  }

  cb(null, true); 
}

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

export default upload;

export const uploadToDb = multer({
  storage: memory,
  fileFilter: fileFilter
}); 
