import knex from "knex";
import config from "../knexfile";
import { NODE_ENV } from "../types/env";
import Bookshelf from "bookshelf";

const environment: NODE_ENV = <NODE_ENV>process.env.NODE_ENV || "development";

export const bookshelf = Bookshelf(knex(config[environment]));
export default knex(config[environment]);
