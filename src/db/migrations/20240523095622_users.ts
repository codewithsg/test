import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("users", table => {
    table.uuid("id").primary().defaultTo(knex.fn.uuid());
    table.string("username", 120).notNullable().unique();
    table.string("email", 120).notNullable();
    table.string("password", 255).notNullable();
    table.timestamp("created", { useTz: true, precision: 2 }).defaultTo(knex.fn.now());
    table.timestamp("updated", { useTz: true, precision: 2 }).defaultTo(knex.fn.now());
  });
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("users");
}

